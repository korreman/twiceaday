import numpy as np
from PIL import Image

np.set_printoptions(edgeitems=30, linewidth=100000)

img = Image.open("res/pyjamas.png").convert("RGB")  # convert to ensure alpha layer is discarded
array = np.array(img, dtype=np.uint16)


def convert(x):
    r, g, b = x
    return (int(r/255*31) << 11) | (int(g/255*63) << 5) | int(b/255*31)  # what the fuck?

# signature ensures the function receives an array of rgb colours instead of individual scalars
vconvert = np.vectorize(convert, signature="(3)->()")
converted = vconvert(array)

long_list = converted.ravel()
final_list = [f"0x{int(x):04x}" for x in long_list]
final_string = ", ".join(final_list)

rust = f"""
// shape: {converted.shape}
pub const IMG: [u16; {converted.size}] = [{final_string}];
"""
print(rust)
