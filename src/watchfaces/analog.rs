use core::f64::consts::PI;

use num_traits::real::Real;

use crate::humantime::{NaiveDateTime, Timelike};
use crate::Graphics;

use super::Watchface;

pub struct AnalogWatchface {
    last_second_x: u8,
    last_second_y: u8,
    last_minute_x: u8,
    last_minute_y: u8,
    last_hour_x: u8,
    last_hour_y: u8,
}

impl AnalogWatchface {
    pub const fn new() -> Self {
        AnalogWatchface {
            last_second_x: 120,
            last_second_y: 120,
            last_minute_x: 120,
            last_minute_y: 120,
            last_hour_x: 120,
            last_hour_y: 120,
        }
    }
}

impl Watchface for AnalogWatchface {
    fn render_background(&mut self, graphics: &mut Graphics) {
        graphics.set_color(0,0,0);
        graphics.clear();
        graphics.set_stroke(3.0);
        graphics.set_color(10, 10, 10);
        graphics.draw_circle(120, 120, 110);
        graphics.set_color(50, 50, 50);
        graphics.draw_line(120, 15, 120, 25);
        graphics.draw_line(120, 240 - 25, 120, 240 - 15);
        graphics.draw_line(15, 120, 25, 120);
        graphics.draw_line(240 - 25, 120, 240 - 15, 120);

    }
    fn render(&mut self, graphics: &mut Graphics, now: NaiveDateTime) {
        let second = now.second() as f64;
        let second_len = 100.0;
        let second_o: f64 = second * (PI / 30.0) - (PI / 2.0);
        let second_x: u8 = (120.0 + second_len * second_o.cos()) as u8;
        let second_y: u8 = (120.0 + second_len * second_o.sin()) as u8;

        let minute = now.minute() as f64;
        let minute_len = 80.0;
        let minute_o: f64 = minute * (PI / 30.0) - (PI / 2.0);
        let minute_x: u8 = (120.0 + minute_len * minute_o.cos()) as u8;
        let minute_y: u8 = (120.0 + minute_len * minute_o.sin()) as u8;

        let hour = now.hour() as f64;
        let hour_len = 60.0;
        let hour_o: f64 = if hour <= 12.0 {
            (hour * (PI / 6.0) - (PI / 2.0)) + ((minute / 12.0) * (PI / 30.0))
        } else {
            ((hour - 12.0) * (PI / 6.0) - (PI / 2.0)) + ((minute / 12.0) * (PI / 30.0))
        };
        let hour_x: u8 = (120.0 + hour_len * hour_o.cos()) as u8;
        let hour_y: u8 = (120.0 + hour_len * hour_o.sin()) as u8;


        graphics.set_color(255, 255, 255);

        graphics.set_stroke(8.0);
        graphics.draw_line(120, 120, hour_x, hour_y);
        graphics.set_stroke(5.0);
        graphics.draw_line(120, 120, minute_x, minute_y);
        graphics.set_stroke(2.0);
        graphics.draw_line(120, 120, second_x, second_y);

        if self.last_hour_x != hour_x || self.last_hour_y != hour_y {
            graphics.set_stroke(8.0);
            graphics.set_color(0,0,0);
            graphics.draw_line(120, 120, self.last_hour_x, self.last_hour_y);
        }
        if self.last_minute_x != minute_x || self.last_minute_y != minute_y {
            graphics.set_stroke(5.0);
            graphics.set_color(0,0,0);
            graphics.draw_line(120, 120, self.last_minute_x, self.last_minute_y);
        }
        if self.last_second_x != second_x || self.last_second_y != second_y {
            graphics.set_stroke(2.0);
            graphics.set_color(0,0,0);
            graphics.draw_line(120, 120, self.last_second_x, self.last_second_y);
        }

        self.last_second_x = second_x;
        self.last_second_y = second_y;
        self.last_minute_x = minute_x;
        self.last_minute_y = minute_y;
        self.last_hour_x = hour_x;
        self.last_hour_y = hour_y;
    }
}
