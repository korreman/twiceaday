use crate::font::Font;
use crate::humantime::{Datelike, NaiveDateTime, Timelike, Weekday};
use crate::Graphics;

use super::pyjamas_bg::IMG;
use super::Watchface;

fn month_repr(m: u32) -> [char; 3] {
    match m {
        1 => ['j', 'a', 'n'],
        2 => ['f', 'e', 'b'],
        3 => ['m', 'a', 'r'],
        4 => ['a', 'p', 'r'],
        5 => ['m', 'a', 'j'],
        6 => ['j', 'u', 'n'],
        7 => ['j', 'u', 'l'],
        8 => ['a', 'u', 'g'],
        9 => ['s', 'e', 'p'],
        10 => ['o', 'k', 't'],
        11 => ['n', 'o', 'v'],
        12 => ['d', 'e', 'c'],
        _ => panic!("{} is not a month", m),
    }
}

fn weekday_repr(d: Weekday) -> [char; 3] {
    match d {
        Weekday::Mon => ['M', 'a', 'n'],
        Weekday::Tue => ['T', 'i', 'r'],
        Weekday::Wed => ['O', 'n', 's'],
        Weekday::Thu => ['T', 'o', 'r'],
        Weekday::Fri => ['F', 'r', 'e'],
        Weekday::Sat => ['L', 'ø', 'r'],
        Weekday::Sun => ['S', 'ø', 'n'],
    }
}

pub struct PyjamasWatchface;

impl PyjamasWatchface {
    pub const fn new() -> Self {
        PyjamasWatchface {}
    }
}

impl Watchface for PyjamasWatchface {
    fn render_background(&mut self, graphics: &mut Graphics) {
        graphics.set_color(0, 0, 0);
        graphics.clear();
        graphics.draw_image(71, 171, 28, 110, &IMG);
    }

    fn render(&mut self, graphics: &mut Graphics, now: NaiveDateTime) {
        // eg. 13:37:04
        let second_0 = now.second() % 10;
        let second_1 = (now.second() - second_0) / 10;
        let minute_0 = now.minute() % 10;
        let minute_1 = (now.minute() - minute_0) / 10;
        let hour_0 = now.hour() % 10;
        let hour_1 = (now.hour() - hour_0) / 10;
        let text = [
            char::from_digit(hour_1, 10).unwrap(),
            char::from_digit(hour_0, 10).unwrap(),
            ':',
            char::from_digit(minute_1, 10).unwrap(),
            char::from_digit(minute_0, 10).unwrap(),
            ':',
            char::from_digit(second_1, 10).unwrap(),
            char::from_digit(second_0, 10).unwrap(),
        ];
        graphics.render_text(18, 120, Font::Large, &text, 0xffff);

        // eg. Man 9. dec
        now.weekday();
        let day_0 = now.day() % 10;
        let day_1 = (now.day() - day_0) / 10;
        now.month();
        let datetext = [
            ' ',
            char::from_digit(day_1, 10).unwrap(),
            char::from_digit(day_0, 10).unwrap(),
            '.',
            ' ',
        ];

        let dayline_y = 165;
        let char_width = Font::Medium.get_shape().0 as u16;
        let spacing = Font::Medium.get_spacing() as u16;
        let start = 48u16;
        let start2 = start + char_width * 3 + spacing * 2;
        let start3 = start2 + char_width * 5 + spacing * 4;
        graphics.render_text(
            start,
            dayline_y,
            Font::Medium,
            &weekday_repr(now.weekday()),
            0x6dcf,
        );
        graphics.render_text(start2, dayline_y, Font::Medium, &datetext, 0x6dcf);
        graphics.render_text(
            start3,
            dayline_y,
            Font::Medium,
            &month_repr(now.month()),
            0x6dcf,
        );
    }
}
