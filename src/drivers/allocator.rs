// Bump pointer allocator implementation
// From https://docs.rust-embedded.org/book/collections/

use core::alloc::{GlobalAlloc, Layout};
use core::cell::UnsafeCell;
use core::mem::{align_of, size_of};
use core::ptr;
use cortex_m::interrupt;

fn align_forward(addr: usize, align: usize) -> usize {
    let mask = align - 1;
    (addr + mask) & !mask
}

// TODO: Handle Out Of Memory (OOM) errors 🤷
// See https://docs.rust-embedded.org/book/collections/

/// A 16-bit representation of an address.
///
/// Since we only have 64KiB RAM, there's no reason to waste 4 bytes on every address we store.
/// Instead, we can use a `u16` and offset the address with a constant.
#[derive(Clone, Copy, Debug)]
struct TinyAddr(u16);

// Addresses are derived from https://infocenter.nordicsemi.com/pdf/nRF52832_PS_v1.4.pdf section 8.
// We are using RAM2, to give space to both RTT and the HEAP.
impl TinyAddr {
    /// Address of the first byte contained in the memory space.
    const START: usize = 0x2000_2000;

    /// Address of the first byte after the memory space.
    /// Not a part of the memory space.
    const END: usize = 0x2000_4000;

    /// Alias for the null address.
    const NULL_ALIAS: u16 = 0xFFFE;

    /// Alias for the address right after the range (`Self::END`).
    const END_ALIAS: u16 = 0xFFFF;

    #[inline]
    fn to_ptr<T>(self) -> *mut T {
        self.to_addr() as *mut T
    }

    #[inline]
    fn to_addr(self) -> usize {
        if self.0 == Self::NULL_ALIAS {
            0
        } else if self.0 == Self::END_ALIAS {
            Self::END
        } else {
            Self::START + self.0 as usize
        }
    }

    #[inline]
    fn from_addr(addr: usize) -> Option<Self> {
        if addr == 0 {
            Some(Self(Self::NULL_ALIAS))
        } else if addr == Self::END {
            Some(Self(Self::END_ALIAS))
        } else if (Self::START..Self::END).contains(&addr) {
            Some(Self((addr - Self::START) as u16))
        } else {
            None
        }
    }

    #[inline]
    fn from_ptr<T>(ptr: *mut T) -> Option<Self> {
        Self::from_addr(ptr as usize)
    }
}

/// A header for storing basic information about a block:
/// - Whether or not the block is free (unallocated).
/// - Addresses of neighboring blocks in contiguous memory space.
///
/// We must have `prev_addr < next_addr` for any valid header.
/// We encode the `free` boolean by putting `prev_addr` in `a` when true, `b` when false.
/// Then `free` can be computed using `a < b`.
/// Note that we also need to swap around addresses as a result.
#[derive(Clone, Copy, Debug)]
struct Block {
    // If free, represents next, otherwise represents prev.
    a: TinyAddr,
    // If free, represents prev, otherwise represents next.
    b: TinyAddr,
}

impl Block {
    #[inline]
    fn new(free: bool, prev: TinyAddr, next: TinyAddr) -> Self {
        if free {
            Self { a: prev, b: next }
        } else {
            Self { a: next, b: prev }
        }
    }

    #[inline]
    fn is_free(&self) -> bool {
        self.a.to_addr() < self.b.to_addr()
    }

    #[inline]
    fn set_free(&mut self, free: bool) {
        if self.is_free() != free {
            core::mem::swap(&mut self.a, &mut self.b);
        }
    }

    #[inline]
    fn get_next(&self) -> TinyAddr {
        if self.is_free() {
            self.b
        } else {
            self.a
        }
    }

    #[inline]
    fn set_next(&mut self, next: TinyAddr) {
        if self.is_free() {
            self.b = next;
        } else {
            self.a = next;
        }
    }

    #[inline]
    fn get_prev(&self) -> TinyAddr {
        if self.is_free() {
            self.a
        } else {
            self.b
        }
    }

    #[inline]
    fn set_prev(&mut self, prev: TinyAddr) {
        if self.is_free() {
            self.a = prev;
        } else {
            self.b = prev;
        }
    }
}

/// A header for storing the addresses of next and previous nodes in the free list.
#[derive(Clone, Copy, Debug)]
struct Link {
    next: TinyAddr,
    prev: TinyAddr,
}

const HEADER_SIZE: usize = size_of::<Block>();
const LINK_SIZE: usize = size_of::<Link>();

#[global_allocator]
pub static HEAP: TinyAlloc = TinyAlloc {
    head: UnsafeCell::new(ptr::null_mut()),
};

/// A small next-fit list allocator.
///
/// This allocator uses the limitation to `2^16` memory space
/// and some tricks to keep the header size at a minimum.
pub struct TinyAlloc {
    // The current "head" of the circular list of free blocks.
    head: UnsafeCell<*mut Block>,
}

impl TinyAlloc {
    pub unsafe fn init(&self) {
        interrupt::free(|_| {
            let start = TinyAddr(0);
            // Point the head to the start of the memory section.
            *self.head.get() = TinyAddr(0).to_ptr();

            // Write block header.
            *start.to_ptr() = Block::new(
                true,
                TinyAddr(TinyAddr::NULL_ALIAS),
                TinyAddr(TinyAddr::END_ALIAS),
            );

            // Write link header.
            *TinyAddr(HEADER_SIZE as u16).to_ptr() = Link {
                next: start,
                prev: start,
            }
        })
    }

    unsafe fn constrain_layout(layout: Layout) -> Layout {
        // Since we write headers in the allocated space,
        // allocated blocks must have at least the size and alignment of the data we write there.
        let size = layout.size().max(LINK_SIZE);
        let align = layout.align().max(align_of::<Block>());
        Layout::from_size_align_unchecked(size, align).pad_to_align()
    }
}

unsafe impl Sync for TinyAlloc {}

unsafe impl GlobalAlloc for TinyAlloc {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        let layout = Self::constrain_layout(layout);
        let req_size = layout.size();
        let req_align = layout.align();
        // avoid interrupts
        interrupt::free(|_| {
            let mut head = *self.head.get();
            if head.is_null() {
                return ptr::null_mut();
            }
            let head_start = head;
            loop {
                // Read the block header.
                let block: Block = *head;

                // We wish to be able to encode header sizes of up to exactly 64KiB, 0x10000 bytes.
                // For that to be possible we choose the convention
                // that a block pointing to itself as next must be 0x10000 large.
                let block_size = block.get_next().to_ptr::<Block>().byte_offset_from(head) as usize;

                // Next, find an aligned address and the available size starting at that address.
                // - Address of the start of the actual memory space.
                let data: *mut Link = head.byte_add(HEADER_SIZE).cast();
                // - First non-header address in block that matches alignment requirements.
                let data_aligned = align_forward(data as usize, req_align) as *mut Link;
                // - Effective size of full block taking alignment into account.
                let aligned_size = block_size - (data_aligned.byte_offset_from(data)) as usize;

                // Read the link to the next free block.
                let link = *data;

                // If aligned size is too small, move on to the next free block in linked list.
                if req_size > aligned_size - HEADER_SIZE {
                    // Go to the next free block.
                    head = link.next.to_ptr();
                    if head == head_start {
                        return ptr::null_mut();
                    }
                    continue;
                }

                // Remove block from linked list.
                let mut new_head: *mut Block = if link.next.to_ptr() == head {
                    // Current block is last block in linked list, point head to nowhere.
                    ptr::null_mut()
                } else {
                    (*link.prev.to_ptr::<Link>().byte_add(HEADER_SIZE)).next = link.next;
                    (*link.next.to_ptr::<Link>().byte_add(HEADER_SIZE)).prev = link.prev;
                    link.next.to_ptr()
                };

                // Align block header so it's placed right before aligned data.
                let head_aligned = data_aligned.byte_sub(HEADER_SIZE).cast::<Block>();
                let head_aligned_tiny = TinyAddr::from_ptr(head_aligned).unwrap_unchecked();

                // Make adjacent blocks point to realigned block header.
                // We'll be writing the header itself in a bit.
                if block.get_prev().to_addr() != 0 {
                    let prev = block.get_prev().to_ptr::<Block>();
                    (*prev).set_next(head_aligned_tiny);
                }
                if block.get_next().to_addr() != TinyAddr::END {
                    let next = block.get_next().to_ptr::<Block>();
                    (*next).set_prev(head_aligned_tiny);
                }

                // If there's room for another block after the data, we create one.
                let split = aligned_size - req_size > HEADER_SIZE + LINK_SIZE;
                // `next` decides where the final header for the allocated block points next.
                let next = if split {
                    let split_addr = align_forward(
                        data_aligned.byte_add(req_size) as usize,
                        align_of::<Block>(),
                    );
                    let split = split_addr as *mut Block;
                    let split_tiny = TinyAddr::from_ptr(split).unwrap_unchecked();

                    // Write new block header
                    *split = Block::new(true, head_aligned_tiny, block.get_next());
                    // Update where `prev` of next block points to.
                    let next = block.get_next().to_ptr::<Block>();
                    if next as usize != TinyAddr::END {
                        (*next).set_prev(split_tiny);
                    }

                    // Insert new block into free list.
                    if !new_head.is_null() {
                        (*link.next.to_ptr::<Link>().byte_add(HEADER_SIZE)).prev = split_tiny;
                        (*link.prev.to_ptr::<Link>().byte_add(HEADER_SIZE)).next = split_tiny;
                        *split.byte_add(HEADER_SIZE).cast::<Link>() = Link {
                            next: link.next,
                            prev: link.prev,
                        };
                    } else {
                        *split.byte_add(HEADER_SIZE).cast::<Link>() = Link {
                            next: split_tiny,
                            prev: split_tiny,
                        };
                    }
                    new_head = split;

                    split_tiny
                } else {
                    block.get_next()
                };

                // Write resulting header for allocated block at the (aligned) header address.
                *head_aligned_tiny.to_ptr() = Block::new(false, block.get_prev(), next);

                // Update position of head
                *self.head.get() = new_head;

                return data_aligned as *mut u8;
            }
        })
    }

    unsafe fn dealloc(&self, block: *mut u8, _: Layout) {
        // avoid interrupts
        interrupt::free(|_| {
            let new_head = block.byte_sub(HEADER_SIZE).cast::<Block>();

            (*new_head).set_free(true);

            // Insert block back into free list.
            let head = *self.head.get();
            if !head.is_null() {
                let current_head = head.byte_add(HEADER_SIZE).cast::<Link>();
                let prev = (*current_head).prev;

                let new_head_tiny = TinyAddr::from_ptr(new_head).unwrap_unchecked();
                (*current_head).prev = new_head_tiny;
                (*prev.to_ptr::<Link>().byte_add(HEADER_SIZE)).next = new_head_tiny;

                // Write link header.
                *new_head.byte_add(HEADER_SIZE).cast::<Link>() = Link {
                    next: TinyAddr::from_ptr(head).unwrap_unchecked(),
                    prev,
                };
            } else {
                let new_head_tiny = TinyAddr::from_ptr(new_head).unwrap_unchecked();
                // Write self-referential header.
                *new_head.byte_add(HEADER_SIZE).cast::<Link>() = Link {
                    next: new_head_tiny,
                    prev: new_head_tiny,
                };
            }

            // Point self.head to block.
            *self.head.get() = new_head;

            // Combine a free block with the previous block.
            let merge_backward = |block: *mut Block| {
                // Get block and its neighbors.
                let block_header = *block;
                let prev: *mut Block = block_header.get_prev().to_ptr();
                let next: *mut Block = block_header.get_next().to_ptr();

                // Combine with previous block (by removing from block list).
                (*prev).set_next(block_header.get_next());
                (*next).set_prev(block_header.get_prev());

                // Remove from free list.
                let free_header = *block.byte_add(HEADER_SIZE).cast::<Link>();
                (*free_header.prev.to_ptr::<Link>().byte_add(HEADER_SIZE)).next = free_header.next;
                (*free_header.next.to_ptr::<Link>().byte_add(HEADER_SIZE)).prev = free_header.prev;
            };

            let next: *mut Block = (*new_head).get_next().to_ptr();
            if next as usize != TinyAddr::END && (*next).is_free() {
                merge_backward(next);
            }
            let prev: *mut Block = (*new_head).get_prev().to_ptr();
            if !prev.is_null() && (*prev).is_free() {
                // Point self.head to previous block.
                *self.head.get() = (*new_head).get_prev().to_ptr();
                merge_backward(new_head);
            }
        });
    }
}
