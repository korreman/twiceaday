use rtt_target::rprintln;

#[derive(Debug)]
pub enum Gesture {
    DoubleClick, // needs further testing
    SlideDown,
    SlideUp,
    SlideLeft,
    SlideRight,
    Click,
    LongPress,
}

#[allow(unused)]
#[derive(Debug)]
pub struct TouchEvent {
    pub x: u8,
    pub y: u8,
    pub gesture: Gesture,
}

impl TouchEvent {
    pub fn parse(arr: &[u8; 63]) -> Option<Self> {
        let gesture = match arr[1] {
            0x00 => Gesture::DoubleClick,
            0x01 => Gesture::SlideDown,
            0x02 => Gesture::SlideUp,
            0x03 => Gesture::SlideLeft,
            0x04 => Gesture::SlideRight,
            0x05 => Gesture::Click,
            0x0C => Gesture::LongPress,
            _ => {
                rprintln!("Bad message from touch device: {:?}", &arr);
                return None;
            }
        };
        let x = arr[4];
        let y = arr[6];
        Some(TouchEvent { x, y, gesture })
    }
}
