pub use chrono::{Datelike, NaiveDateTime, Timelike, Weekday};

use crate::RTCDriver;

pub struct DatetimeProvider {
    rtc: RTCDriver,
    offset: i64,
}

impl DatetimeProvider {
    pub fn new(rtc: RTCDriver, offset: i64) -> Self {
        DatetimeProvider {
            rtc,
            offset,
        }
    }

    pub fn now(&self) -> NaiveDateTime {
        NaiveDateTime::from_timestamp_millis(self.offset + self.rtc.uptime() as i64 * 1000).unwrap()
    }
}
